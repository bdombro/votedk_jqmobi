STATE = {
    tRound: 1,
    oProgress: {1: false, 2: false, 3: false, 4: false, 5: false, 6: false}
}

 
function fCheckIfVoteComplete() {
    for(var i in STATE.oProgress) {
        if(STATE.oProgress[i]===false) {
            console.log(i+':'+STATE.oProgress[i]);
            return false;
        }
    }
    return true;
}

function fChangePanelListColor(tVoteNumber, color) {
	if(tVoteNumber==-1) {
		$('#panelList li').css('background',color);
	    $('#panelList li > a').css('background',color);
	    $('#panelList li > a > div > text').css('background',color);
	}
	else {
		$('#panelList li:nth-child('+tVoteNumber+')').css('background',color);
	    $('#panelList li:nth-child('+tVoteNumber+') > a').css('background',color);
	    $('#panelList li:nth-child('+tVoteNumber+') > a > div > text').css('background',color);
	}
}

function fSaveVote(tVoteNumber) {
    if(STATE.oProgress[tVoteNumber]===false) {
        STATE.oProgress[tVoteNumber]=true;
        $( "#voteProgressBar" ).val($( "#voteProgressBar" ).val() + 100/6);
        fChangePanelListColor(tVoteNumber, '#d8c7c7');
    }
    if(fCheckIfVoteComplete()) {
        $("#submit-block-button").hide();
        $("#submit-button").show();
    }
}

function fSubmitVote() {
    STATE.tRound+=1;
    //alert('Thanks! On to Round '+STATE.tRound);
    
    //Reset everything
    STATE.oProgress = {1: false, 2: false, 3: false, 4: false, 5: false, 6: false};
    fChangePanelListColor(-1,'#ffffff')
    $( "#voteProgressBar" ).val(0);
    $("#submit-block-button").show();
    $("#submit-button").hide();
    $("#roundHeader").html("Round "+STATE.tRound);
}


//This is additional code to what is in init.js, but makes more sense to put here
$(document).ready(function(){
    $("#roundHeader").html("Round "+STATE.tRound);
    fChangePanelListColor(-1,'#ffffff');
});