
//include touch.js on desktop browsers only ------------------------------>

if(!((window.DocumentTouch&&document instanceof DocumentTouch)||'ontouchstart' in window)){
    var script=document.createElement("script");
    script.src="js/jq.desktopBrowsers.js";
    var tag=$("head").append(script);
}

$(document).ready(function(){

});



//This function runs once the page is loaded, but appMobi is not yet active ------------>
var webRoot="./";
$.ui.autoLaunch=false;
$.ui.resetScrollers=false;
var init = function(){
   $.ui.backButtonText="Back";  
   window.setTimeout(function(){$.ui.launch();},1500);
   //$.ui.removeFooterMenu(); This would remove the bottom nav menu
};
document.addEventListener("DOMContentLoaded",init,false);  
$.ui.ready(function(){console.log('ready');});
   
    
    
/* This code is used to run as soon as appMobi activates */
var onDeviceReady=function(){
    AppMobi.device.setRotateOrientation("portrait");
    AppMobi.device.setAutoRotate(false);
    webRoot=AppMobi.webRoot+"/";
    //hide splash screen
    AppMobi.device.hideSplashScreen();  
    
};
document.addEventListener("appMobi.device.ready",onDeviceReady,false);    